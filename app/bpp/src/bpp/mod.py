"""Internal module in bpp"""

import lib


def parts():
    return [lib.things(), lib.nested()]
