"""Main entrypoint for app"""

from . import mod


def main():
    for part in mod.parts():
        print(part)


if __name__ == "__main__":
    main()
