#!/bin/sh

for pattern in 'deps/**/pyproject.toml' 'lib/**/pyproject.toml' 'app/**/pyproject.toml'; do
	git ls-files -z --exclude-standard "$pattern" |
		xargs -0 dirname
done
