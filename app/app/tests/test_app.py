from app import mod

def test_parts():
    for part in mod.parts():
        assert part is not None
