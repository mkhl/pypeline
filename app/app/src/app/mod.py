"""Internal module in app"""

import lia
import lib


def parts():
    return [lia.stuff(), lib.things(), lib.nested()]
